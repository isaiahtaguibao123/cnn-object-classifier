import os
from pathlib import Path
import numpy as np
import tensorflow as tf
import pandas as pd
import cv2
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle

def process_image(image_path, image_width, image_height):
	img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
	img = cv2.resize(img, (image_width, image_height))
	img = cv2.normalize(img, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F, dst=None)
	img = np.array(img)
	return img

def process_training_image(train_dir, image_width, image_height):
	training_data = []
	label_data = []
	dirpath = os.path.basename(train_dir)
	print(dirpath)
	for train_img in os.listdir(train_dir):
		if dirpath == 'arch':
			label_data.append([1, 0, 0, 0, 0])
		elif dirpath == 'left_loop':
			label_data.append([0, 1, 0, 0, 0])
		elif dirpath == 'right_loop':
			label_data.append([0, 0, 1, 0, 0])
		elif dirpath == 'tented_arch':
			label_data.append([0, 0, 0, 1, 0])
		else:
			label_data.append([0, 0, 0, 0, 1])
		image_path = os.path.join(train_dir, train_img)
		img = process_image(image_path, image_width, image_height)
		training_data.append(img)
	training_data, label_data = shuffle(np.array(training_data), np.array(label_data))
	return training_data, label_data

def prepare_train_test_set(x_inputs, y_inputs, split_ratio=0.10):
	return train_test_split(x_inputs, y_inputs, test_size=split_ratio)

class CNN():
	def __init__(self, img_width, img_height, img_channel, n_outputs, model_path=None, summary_path=None):
		self.X = tf.placeholder(tf.float32, shape=[None, img_width, img_height, img_channel], name="input")
		self.y = tf.placeholder(tf.float32, shape=(None, n_outputs))
		self.mode = tf.placeholder(tf.bool)
		self.n_outputs = n_outputs
		self.model_path = model_path

		self.tf_graph = tf.Graph().as_default()
		self.sess = tf.Session()
		
		self._build_network()
		self.sess.run(tf.global_variables_initializer())

		self.saver = tf.train.Saver()

		if model_path is not None:
			print(Path(model_path+'.meta').exists())
			if(Path(model_path+'.meta').exists()):
				print("model loaded")
				self.saver.restore(self.sess, model_path)

		if summary_path is not None:
			self.summaries = tf.summary.merge_all()
			self.writer = tf.summary.FileWriter(summary_path)
			self.writer.add_graph(self.sess.graph)

	def _build_network(self):
		with tf.name_scope("neural_network"):
			self.conv1 = self._convolution(self.X, filters=64, kernel_size=[5, 5], name="conv1")
			self.conv2 = self._convolution(self.conv1, filters=64, kernel_size=[5, 5], name="conv2")
			self.pool1 = self._pooling(self.conv1+self.conv2, pool_size=[2, 2], strides=2, name="pool1")

			self.conv3 = self._convolution(self.pool1, filters=128, kernel_size=[5, 5], name="conv3")
			self.conv4 = self._convolution(self.conv3, filters=128, kernel_size=[5, 5], name="conv4")
			self.pool2 = self._pooling(self.conv3+self.conv4, pool_size=[2, 2], strides=2, name="pool2")

			self.conv5 = self._convolution(self.pool2, filters=256, kernel_size=[3, 3], name="conv5")
			self.conv6 = self._convolution(self.conv5, filters=256, kernel_size=[3, 3], name="conv6")
			self.pool3 = self._pooling(self.conv5+self.conv6, pool_size=[2, 2], strides=2, name="pool3")

			self.conv7 = self._convolution(self.pool3, filters=512, kernel_size=[3, 3], name="conv7")
			self.conv8 = self._convolution(self.conv7, filters=512, kernel_size=[3, 3], name="conv8")
			self.pool4 = self._pooling(self.conv7+self.conv8, pool_size=[2, 2], strides=2, name="pool4")

			self.g_avg_pool = tf.reduce_mean(self.pool4, [1,2])

			# self.flatten = tf.layers.flatten(self.pool3)
			# self.fc1 = tf.layers.dense(self.g_avg_pool, units=1024, activation=tf.nn.relu, name="fc1")
			# self.dropout1 = tf.layers.dropout(self.fc1, rate=0.4, training=self.mode)
			self.logits = tf.layers.dense(self.g_avg_pool, units=self.n_outputs, name="outputs")
			self.probabilities = tf.nn.softmax(self.logits, name="probabilities")

		with tf.name_scope("loss"):
			self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=self.y, logits=self.logits))
			# self.loss = tf.losses.softmax_cross_entropy(self.y, self.logits)
			tf.summary.scalar("loss", self.loss)
		with tf.name_scope("accuracy"):
			# self.accuracy = tf.metrics.accuracy(self.y, self.logits)
			correct_prediction = tf.equal(tf.argmax(self.logits, 1), tf.argmax(self.y, 1))
			self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
			tf.summary.scalar("accuracy", self.accuracy)
		with tf.name_scope("train"):
			self.train_op = tf.train.AdamOptimizer().minimize(self.loss)

	def _convolution(self, input, filters, kernel_size, strides=1, padding="same", name="conv"):
		return tf.layers.conv2d(input, filters=filters, kernel_size=kernel_size, strides=strides, padding=padding, activation=tf.nn.relu, name=name)

	def _pooling(self, input, pool_size, strides=1, padding="same", name="pool"):
		return tf.layers.max_pooling2d(input, pool_size=pool_size, strides=strides, padding=padding, name=name)

	def train(self, X, y, x_test, y_test, n_epochs, batch_size):
		total_batch = len(X) // batch_size
		total_test_batch = len(X_test) // batch_size
		test_batch_i = 0

		for epoch in range(n_epochs):
			if test_batch_i > total_test_batch:
				test_batch_i = 0

			current_batch = {'X': [], 'y': []}
			batch_i = 0
			for i in range(total_batch):
				current_batch['X'] = X[batch_i: batch_i + batch_size]
				current_batch['y'] = y[batch_i: batch_i + batch_size]

				self.sess.run(self.train_op, feed_dict={self.X: current_batch['X'], self.y: current_batch['y'], self.mode: True})
			if (total_batch * batch_size) != len(X):
				last_batch = len(X) - (total_batch * batch_size)
				self.sess.run(self.train_op, feed_dict={self.X: X[-last_batch:], self.y: y[-last_batch:], self.mode: True})
			# s = self.sess.run(self.summaries, feed_dict={self.X: X[:batch_size], self.y: y[:batch_size], self.mode: False})
			if epoch % 10 == 0:
				train_loss = self.sess.run(self.loss, feed_dict={self.X: X[:batch_size], self.y: y[:batch_size], self.mode: True})
				train_accuracy = self.sess.run(self.accuracy, feed_dict={self.X: X[:batch_size], self.y: y[:batch_size], self.mode: True})
				accuracy = self.sess.run(self.accuracy, feed_dict={self.X: x_test[test_batch_i: test_batch_i + batch_size], self.y: y_test[test_batch_i: test_batch_i + batch_size], self.mode: False})
				print("Epoch: {}, accuracy: {}, training accuracy: {}, training loss: {}".format(epoch, accuracy, train_accuracy, train_loss))
			if epoch % 100 == 0:
				self.saver.save(self.sess, self.model_path)
			test_batch_i += 1

	def predict(self, input):
		return self.probabilities.eval(feed_dict={self.X: input, self.mode: False})

	def rotate_images(images, degrees=45):
		pass

	def flip_images(images):
		pass

if __name__ == '__main__':
	train_dir = ['./train/arch', './train/left_loop', './train/right_loop', './train/tented_arch', './train/whorl']
	MODEL_PATH = './model/model.ckpt'
	SUMMARY_PATH = './summary'
	training_data = []
	label_data = []

	IMAGE_WIDTH = 128
	IMAGE_HEIGHT = 128
	for directory in train_dir:
		x_data, y_data = process_training_image(directory, IMAGE_WIDTH, IMAGE_HEIGHT)
		training_data.append(x_data)
		label_data.append(y_data)
	training_data = np.array(training_data)
	label_data = np.array(label_data)
	# training_data = np.vstack(training_data)
	# label_data = np.vstack(label_data)
	training_data = training_data.reshape(training_data.shape[0] * training_data.shape[1], training_data.shape[2], training_data.shape[3], 1)
	label_data = label_data.reshape(label_data.shape[0] * label_data.shape[1], label_data.shape[2])
	print(training_data.shape)
	print(label_data.shape)
	X_train, X_test, y_train, y_test = prepare_train_test_set(training_data, label_data)
	print(X_train.shape, X_test.shape, y_train.shape, y_test.shape)
	cnn = CNN(IMAGE_WIDTH, IMAGE_HEIGHT, 1, 5, MODEL_PATH, SUMMARY_PATH)
	cnn.train(X_train, y_train, X_test, y_test, 1000, 32)